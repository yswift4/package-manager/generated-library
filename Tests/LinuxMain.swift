import XCTest

import GeneratedLibraryTests

var tests = [XCTestCaseEntry]()
tests += GeneratedLibraryTests.allTests()
XCTMain(tests)