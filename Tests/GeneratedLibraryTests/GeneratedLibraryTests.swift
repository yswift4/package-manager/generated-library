import XCTest
@testable import GeneratedLibrary

final class GeneratedLibraryTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(GeneratedLibrary().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
